from flask import Flask, request, render_template, redirect, url_for
from flask_mongodb.mongo_for_flask import MongoHandler

################################################
# flask_app.py : Flask application for a simple
# working address book.
# imports are required
################################################

# MongoHandler is used in order to communicate with the database
# NOTE: database is 'incubator' and collection is 'FLASK' (specified in mongo_for_flask.py)
handler = MongoHandler()

# initialize the app
app = Flask(__name__)


# simple checking for user insertion
def check_doc(doc):
    if '_id' not in doc or 'name' not in doc or 'phone' not in doc or 'email' not in doc:
        return False
    return True


# default route in the app
@app.route("/")
def index():
    return render_template("index.html"), 200


# /users route with GET will return all users in the collection
@app.route('/users', methods=["GET"])
def get_all_users():
    res = handler.display_entries()  # dump the result with 200 OK success code
    if res.count() != 0:
        # returns result in users_table.html template
        return render_template("users_table.html", users=res, user_id=-1), 200
    else:
        return "No users in database", 404


# the Post method in /users path will add a new user
@app.route('/users', methods=["POST"])
def insert_user():
    data = request.get_json()
    if data is not None:  # check that request body comes as application/json
        if check_doc(data):
            res = handler.insert_new_entry(data)  # use handler to insert a new user
            if res == 1:
                return "successful insertion"
            elif res == -1:
                return "cannot insert duplicate key", 500
            else:
                return "error", 500
        else:
            return "not valid data", 500
    else:  # this will occur when creating a user in the template
        # manually create input data from request.form
        data = {'_id': request.form['IdInput'], 'name': request.form['NameInput'],
                'phone': request.form['PhoneInput'], 'email': request.form['EmailInput']}
        res = handler.insert_new_entry(data)  # use handler to insert a new user
        if res == 1:
            return "successful insertion"
        elif res == -1:
            return "cannot insert duplicate key", 500
        else:
            return "error", 500


# GET method in /users/<id> will return single user
# if one with the given id exists, else an error
# 404 will occur
@app.route('/users/<int:user_id>', methods=["GET"])
def get_user(user_id):
    res = handler.display_entries(str(user_id))
    if res.count() != 0:
        return render_template("users_table.html", users=res, user_id=user_id), 200
    else:
        return "id not found", 404


# PUT method in /users/<id> is used to update a user with given id
@app.route('/users/<int:user_id>', methods=["PUT"])
def update_user(user_id):
    data = request.get_json()
    if data is not None:  # check that request came as application/json
        res = handler.update_entry(str(user_id), data)
        if res['ok'] == 1 and res['nModified'] == 1:  # user found and updated
            return "update successfull", 200  # returns with 200 OK code
        else:
            return "id not found to be updated", 404  # else if not found return with 404
    else:
        return "data not in json format", 500


# DELETE method in /users/<id> will request deletion of that user if found
@app.route('/users/<int:user_id>', methods=["DELETE"])
def delete_user(user_id):
    res = handler.delete_entries(str(user_id))
    if res == 1:  # if number of users deleted equals 1 then its successfull
        return "deletion successful", 200
    else:  # else id could not be found
        return "deletion unsuccessful, id not found", 404


# helping path for user creation template
@app.route('/user_input')
def user_input():
    return render_template('user_input_form.html'), 200


# helping path for user find template
# if method is post, then form is submitted and we redirect to get_user
@app.route('/user_find', methods=["GET", "POST"])
def user_find():
    if request.method == "GET":
        return render_template('user_find_form.html'), 200
    else:
        return redirect(url_for('get_user', user_id=int(request.form["IdInput"])))


app.run(port=7676)  # run the app on localhost (127.0.0.1) in port 7676
