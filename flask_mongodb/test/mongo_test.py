import flask_mongodb.mongo as mon
from bson.json_util import dumps

entries = [{"firstname": "Nick", "lastname": "Jones", "email": "njones@domain.ex", "phone": {"work": "1234567890"}},
           {"firstname": "Nickolas", "lastname": "Dunston"},
           {"firstname": "Gregory", "lastname": "Dunlop"},
           {"firstname": "Jenny", "lastname": "Payton"},
           {"firstname": "Maria", "lastname": "Dukes"},
           ]


mon.delete_entries()
mon.display_entries()

for en in entries:
    mon.insert_new_entry(en)
mon.display_entries()

mon.update_entry("Nickolas", "Dunston", {"email": "this@that.at"})
mon.display_entries("Nick")

mon.delete_entries(None, "Dunlop")
mon.display_entries()
