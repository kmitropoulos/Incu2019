from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError
from json import load

############################################
# mongo_for_flask.py: file containing the
# class in order for flask_app to access
# mongodb.
# imports are required
############################################


# the full path of the config file should be here
config = 'D:/program files/Git/Incubator/Incu2019/flask_mongodb/config.json'


# read the server ip address and port from the config file
def read_config():
    try:
        with open(config) as file:
            conf = load(file)
            server_ip = conf['server_ip']
            server_port = conf['server_port']
            return [server_ip, server_port]
    except:
        print("Error in config loading")
    return


# connect to the database and return the handler
# using the info from config file
def get_mongo():
    cred = read_config()  # read config
    client = MongoClient(cred[0], int(cred[1]))
    db = client['incubator']  # use the db 'incubator'
    return db.FLASK  # and the collection 'FLASK'


# class responsible to communicate with db
class MongoHandler:

    # get the handle to the collection
    def __init__(self):
        self.table = get_mongo()

    # insert an entry.
    def insert_new_entry(self, doc):
        try:
            self.table.insert_one(doc)
        except DuplicateKeyError:  # will happen if '_id' in doc already exists in 'FLASK'
            return -1
        except:
            return 0
        return 1

    # update an entry with given id and set of values
    def update_entry(self, user_id, values):
        res = self.table.update_one({'_id': user_id}, {'$set': values})
        return res.raw_result  # returns raw_result for flask_app to check if it was successful

    # display entries according with input given
    # will display all if no id is given, or will search for given id
    def display_entries(self, user_id=None):
        if user_id is None:
            res = self.table.find({})
        else:
            res = self.table.find({'_id': user_id})
        return res

    # delete entries having the given id
    def delete_entries(self, user_id):
        res = self.table.delete_one({'_id': user_id})
        return res.deleted_count  # returns the counter of deleted entries, should be 0 or 1
