from pymongo import MongoClient
from bson.json_util import dumps


def get_mongo():
    print('Connecting')
    # Old line: client = MongoClient("127.0.0.1", 270017)
    # error in the port number. New line
    client = MongoClient("127.0.0.1", 27017)
    # Old line: return client['my_nice_database']
    # New lines:
    db = client['my_nice_database']
    print('Connected')
    return db.hosts


def display_row(r):
    print('------- Database Dump --------')    
    for i in r:
        print(dumps(i))
    print('------------------------------')


def main_code():
    # Old lines:
    # db = get_mongo()
    # table = db.hosts
    # New Line:
    table = get_mongo()

    # Clean start, deleting everything from the table.
    table.delete_many({})

    # Inserting 5 routers into the table

    rows_to_insert = [{'hostname': 'VeryImportantRouter', 'ip': '1.1.1.1', 'domain': 'Incubator.com'},
                      {'hostname': 'VeryImportantBackupRouter', 'ip': '1.1.1.2', 'domain': 'Incubator.com'},
                      {'hostname': 'NotSoImportantRouter', 'ip': '1.1.1.3', 'domain': 'Incubator.com'},
                      {'hostname': 'NetFlixRouter', 'ip': '1.1.1.4', 'domain': 'Home.com'},
                      {'hostname': 'CoreRouter', 'ip': '1.1.1.5', 'domain': 'Home.com'}]

    table.insert_many(rows_to_insert)

    # Display all database
    print('\nDisplaying all entries in DB')
    res = table.find()
    display_row(res)    

    # Update the IP address of the very important router to 2.2.2.2 and dump the DB after that
    print('\nAfter update of IP address')
    # Old line:
    # res = table.update_one({'Hostname':'VeryImportantRouter'}, {'$set': {'ip': '2.2.2.2'}})
    # error in the key 'Hostname'. it should be hostname with lowercase h. New line:
    table.update_one({'hostname': 'VeryImportantRouter'}, {'$set': {'ip': '2.2.2.2'}})
    res = table.find()
    display_row(res)

    # Listing only the Important routers    
    print('\nDisplaying only the important routers')
    # Old line:
    # res = table.find({'hostname': {'$regex': 'ImportantRouter.'}})
    # error in the regex expression. one that will work is '^VeryImportant.*' . New line:
    res = table.find({'hostname': {'$regex': '^VeryImportant.*'}})
    display_row(res)


main_code()
