from pymongo import MongoClient
from json import load
from bson.json_util import dumps

# the full path of the config file should be here
config = 'D:/program files/Git/Incubator/Incu2019/flask_mongodb/config.json'


# read the server ip address and port from the config file
def read_config():
    try:
        with open(config) as file:
            conf = load(file)
            server_ip = conf['server_ip']
            server_port = conf['server_port']
            return [server_ip, server_port]
    except:
        print("Error in config loading")
    return


# connect to the database and return the handler
def get_mongo():
    cred = read_config()
    client = MongoClient(cred[0], int(cred[1]))
    db = client['incubator']
    return db.PEOPLE


# insert an entry. firstname and lastname must be included
def insert_new_entry(doc):
    if 'firstname' not in doc or 'lastname' not in doc:
        print('Invalid document, \'firstname\' or \'lastname\' is not present.')
        return
    table = get_mongo()
    table.insert_one(doc)
    return


# update an entry with given firstname, lastname
def update_entry(firstname, lastname, values):
    table = get_mongo()
    table.update_one({'firstname': firstname, 'lastname': lastname},
                     {'$set': values})
    return


# display entries according with input given
def display_entries(firstname=None, lastname=None):
    if firstname is None:
        first_reg = ".*"  # if firstname is none, accept all
    else:
        first_reg = firstname + ".*"  # else accept everything starting with it
    if lastname is None:  # same as first name
        last_reg = ".*"
    else:
        last_reg = lastname + ".*"
    table = get_mongo()
    # get result sorted on lastname in ascending order
    res = table.find({'firstname': {'$regex': first_reg}, 'lastname': {'$regex': last_reg}}).sort('lastname', 1)
    print(dumps(res))
    return


# delete entries having the given first name, last name
def delete_entries(firstname=None, lastname=None):
    filt = {}  # initialize empty filter in case both inputs are none
    if firstname is not None:
        filt['firstname'] = firstname  # if first name is specified add it to filter
    if lastname is not None:
        filt['lastname'] = lastname  # same as first name
    table = get_mongo()
    table.delete_many(filt)
    return
