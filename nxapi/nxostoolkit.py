import json
import requests
import re

###########################################################
# nxostoolkit library that contains class Nexus.
# Each object can check the status (get_interface_status())
# and upload a description ( configure_interface_desc() )
# for a physical interface, as well as return the platform
# and the version of the switch.
###########################################################


# DN dictionary with paths for Model Information Table access
dn = {"login": "aaaLogin",
      "ethernet_info": "sys/intf/phys-[{int_id}]/phys",
      "ethernet_conf": "sys/intf/phys-[{int_id}]"
      }

# Http headers used
http_headers = {'json': {'content-type': 'application/json'},
                'json-rpc': {'content-type': 'application/json-rpc'}}

# Basic path for the NXAPI-REST
base_path = 'http://{server}:{port}/api/mo/{path}.json'

# Basic path for the NXAPI-CLI JSON-RPC
base_path_rpc = 'http://{server}:{port}/ins'

# Configuration file
# Config path must always be visible
config = 'D:/program files/Git/Incubator/Incu2019/nxapi/config.json'


class Nexus:

    # instantiation of Nexus objects and its fields
    def __init__(self):
        self.version = ''
        self.platform = ''
        self.__token = None  # holds cookie for requests

        # each object reads from config file, so it is possible to have multiple
        # objects with different configurations without recompiling
        try:
            with open(config) as file:
                conf = json.load(file)
                self.server = conf['server']
                self.port = conf['port']

                # when instantiated an object gets the version and the platform of the switch
                self.get_version_platform(conf['username'], conf['password'])
        except:
            print("Error in config loading, please specify server and port (set_server_and_port())\n" +
                  ", and read version and platform manualy (get_version_platform())")

    # Method to manually set server and port for an object
    def set_server_and_port(self, server: str, port: int):
        if type(server) != str or type(port) != int:
            print("server must be a string and port must be int")
        self.server = server
        self.port = port

    # Method to authenticate with the switch using NX-API REST
    def authenticate(self, username: str, password: str):

        # type checking for the input
        if type(username) != str or type(username) != str:
            print("username and password must be strings!")
            return

        # preparing the content of the post request with the contents of the object
        uri = base_path.format(server=self.server, port=self.port, path=dn["login"])
        payload = {"aaaUser": {"attributes": {"name": username, "pwd": password}}}

        # sending the request to the server
        try:
            response = requests.post(uri,
                                     data=json.dumps(payload),
                                     headers=http_headers['json'])
        except:
            print("Connection with {server} could not be established".format(server=self.server))
            return

        # if authentication does not happen we need to check the object credentials
        if response.status_code == 200:
            self.__token = response.cookies
        else:
            print("Authentication Error")

    # method to read the administrative status of a physical interface using NX-API REST
    def get_interface_status(self, interface: str):

        # Checking input so that it is either Eth1/X,
        # Ethernet1/X or Ethernet1/X. If X is not represented from a port
        # we will get status unknown
        pattern = re.compile('^(Eth1|Ethernet1|Ethernet 1)/[0-9]{1,3}$')
        if type(interface) != str or pattern.fullmatch(interface) is None:
            print("Interface Input must be of type:\n1)Eth1/#\n2)Ethernet1/#\n3)Ethernet 1/#")
            return

        # preparing the request
        uri = base_path.format(server=self.server, port=self.port, path=dn["ethernet_info"])\
                       .format(int_id=''.join(['eth1/', interface.split('/')[1]]))

        try:
            response = requests.get(uri, headers=http_headers['json'], cookies=self.__token)
        except:
            print("Connection with {server} could not be established".format(server=self.server))
            return

        # A port may not exist. As a result we will get unkown status.
        # Also authorization cookie might time out, so we need to get
        # authorized again
        response_dict = json.loads(response.text)
        if response.status_code == 200:
            if not response_dict['imdata']:
                print(interface, ': administrative state = unknown')
            else:
                print(interface, ': administrative state =',
                      response_dict['imdata'][0]['ethpmPhysIf']['attributes']['adminSt'])
        elif response.status_code == 403:
            print("Unauthorized or authorization cookie expired. Authorization required")
        elif response.status_code == 400:
            print("Bad Path")
        else:
            print("Access Error")

    # method to change description of a physical interface using NX-API REST
    def configure_interface_desc(self, interface: str, description: str):

        # Same checking as previous method, also description type checked
        pattern = re.compile('^(Eth1|Ethernet1|Ethernet 1)/[0-9]{1,3}$')
        if type(interface) != str or pattern.fullmatch(interface) is None:
            print("Interface Input must be of type:\n1)Eth1/#\n2)Ethernet1/#\n3)Ethernet 1/#")
            return
        if type(description) != str:
            print("Description must be a string")
            return

        # preparing uri and body of the request
        uri = base_path.format(server=self.server, port=self.port, path=dn["ethernet_conf"]) \
                       .format(int_id=''.join(['eth1/', interface.split('/')[1]]))
        payload = {"l1PhysIf": {"attributes": {"descr": description}}}

        try:
            response = requests.post(uri,
                                     data=json.dumps(payload),
                                     headers=http_headers['json'],
                                     cookies=self.__token)
        except:
            print("Connection with {server} could not be established".format(server=self.server))
            return

        # possible configuration errors
        if response.status_code == 200:
            pass
        elif response.status_code == 104:
            print("Invalid Interface")
        elif response.status_code == 403:
            print("Unauthorized or authorization cookie expired. Authorization required")
        elif response.status_code == 400:
            print("Bad Path")
        else:
            print("Configuration Error")

    # method using NXAPI-CLI JSON-RPC to get version and platform of the switch
    def get_version_platform(self, username: str, password: str):

        # requires username and password since it authenticates with different method
        # than authentication above
        if type(username) != str or type(password) != str:
            print("Username and password must be strings")
            return

        # different path from before (base_path)
        uri = base_path_rpc.format(server=self.server, port=self.port)
        payload = [{"jsonrpc": "2.0", "method": "cli",
                    "params": {"cmd": "show version", "version": 1},
                    "id": 1}]

        try:
            response = requests.post(uri,
                                     data=json.dumps(payload),
                                     headers=http_headers['json-rpc'],
                                     auth=(username, password))
        except:
            print("Connection with {server} could not be established".format(server=self.server))
            return

        if response.status_code == 200:
            response_dict = json.loads(response.text)
            self.version = response_dict['result']['body']['kickstart_ver_str']
            self.platform = response_dict['result']['body']['chassis_id']
        elif response.status_code == 400:
            print("Invalid request")
        elif response.status_code == 401:
            print("Authorization required")
        else:
            print("Access Error")
