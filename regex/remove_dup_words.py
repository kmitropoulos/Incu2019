import re

###################################################
# Duplicate words 
# The students will have to find the duplicate words in the given text,
# and return the string without duplicates.
#
###################################################

# A function that removes duplicate words from given text
# Probably exists a better implementation without exclusive usage
# of regex(and possibly also with regex!).


def remove_duplicates(text: str):
    # checking that input is string
    if type(text) != str:
        print("Input must be string")
        return

    # final will contain the unique words we found
    final = []

    # using regex we get the first word (any non-space char within two borders)
    next_word = re.search(r"\b[^\s]+?\b", text)

    # until we get every word possible
    while next_word is not None:

        # create a regular expression searching for the specific word we found
        pat = r"\b" + next_word.group() + r"\b"

        # add the word in the list
        final.append(next_word.group())

        # remove every occurrence of the word from the text
        text = re.sub(pat, "", text)

        # get next word and repeat the process
        next_word = re.search(r"\b[^\s]+?\b", text)

    # transform list to string and return the result
    text_with_no_dup = ' '.join(final)
    return text_with_no_dup

