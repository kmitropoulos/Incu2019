import re

###################################################
# Parse internal CRC counters of a Nexus
# The students will have to parse the next output, and return a dictionary,
# with the parsed info
# 
# How the text input will look like:
#
# --------------------------------------------------------------------------------
# Port          Align-Err    FCS-Err   Xmit-Err    Rcv-Err  UnderSize OutDiscards
# --------------------------------------------------------------------------------
# Eth1/1                0          0          0          0          0           0
# Eth1/2                0          0          0          0          0           0
# Eth1/3                0          0          0          0          0           0
# Eth1/4                0          0          0          0          0           0
# <snip>
#
# --------------------------------------------------------------------------------
# Port         Single-Col  Multi-Col   Late-Col  Exces-Col  Carri-Sen       Runts
# --------------------------------------------------------------------------------
# Eth1/1                0          0          0          0          0           0
# Eth1/2                0          0          0          0          0           0
# <snip>
#
# --------------------------------------------------------------------------------
# Port          Giants SQETest-Err Deferred-Tx IntMacTx-Er IntMacRx-Er Symbol-Err
# --------------------------------------------------------------------------------
# Eth1/1             0          --           0           0           0          0
# Eth1/2             0          --           0           0           0          0
#
# How the output must look like:
#
# int_dict = {
# 	"Eth1/1": {
# 		"Align-Err": 0,
# 		"FCS-Err": 0,
# 		...
# 	}
# }
###################################################


def get_error_counters(text):
    interfaces_dict = {}
    category_list = []  # for the error categories
    for line in text.split('\n'):  # read each line from the text

        if re.match(r"^[\s-]|^$", line) is not None:  # skip empty or ---- lines
            continue

        elif re.match(r"Port", line) is not None:  # each time a line starts with Port
            category_list = re.findall(r"\b\S+\b", line)[1:]  # update category list (DO NOT keep "port" word)

        elif re.match(r"Eth", line) is not None:  # when a line starts with Eth
            eth_and_errors = re.findall(r"\b\S+\b|-+", line)  # get all data from the line (keep "eth" )

            if eth_and_errors[0] in interfaces_dict:  # if the dictionary for the interface exists
                for i in range(0, len(category_list)):  # update it for each pair of category and value
                    # list has one less element since we do not keep "port" in the start of the list
                    interfaces_dict[eth_and_errors[0]][category_list[i]] = eth_and_errors[i + 1]

            else:  # if the interface does not exist already
                # create the key and assign the proper dictionary to it
                interfaces_dict[eth_and_errors[0]] = \
                    {category_list[i]: eth_and_errors[i + 1] for i in range(0, len(category_list))}
        else:
            continue
    return interfaces_dict
