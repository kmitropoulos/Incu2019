import re

###################################################
# IP address validator 
# details: https://en.wikipedia.org/wiki/IP_address
# Student should enter function on the next lines.
# 
# 
# First function is_valid_ip() should return:
# True if the string inserted is a valid IP address
# or False if the string is not a real IP
# 
# 
# Second function get_ip_class() should return a string:
# "X is a class Y IP" or "X is classless IP" (without the ")
# where X represent the IP string, 
# and Y represent the class type: A, B, C, D, E
# ref: http://www.cloudtacker.com/Styles/IP_Address_Classes_and_Representation.png
# Note: if an IP address is not valid, the function should return
# "X is not a valid IP address"
###################################################

# Regular expression used to validate an ipv4 address. Composed of two non-capturing groups.
# The first group is responsible for matching the the three first address bytes ({3}) along with
# the dots (\.), while the last group matches the last byte with the end of string ($)
# each byte can either be:
# a) single digit (\d) (assumed the 0.0.0.0 should pass as valid)
# b) double digit, with the first not being 0 ([1-9]\d)
# c) triple digit in the 100-199 range (1\d\d)
# d) triple digit in the 200-249 range (2[0-4]\d)
# e) triple digit in the 250-255 range (25[0-5])
# The last two cases are separated in order to invalidate 256,257 etc.
valid_reg_ex = r"^(?:\d\.|[1-9]\d\.|1\d\d\.|2[0-4]\d\.|25[0-5]\.){3}(?:\d$|[1-9]\d$|1\d\d$|2[0-4]\d$|25[0-5]$)"


# Validation function
# Accepts ip as a string and tries to match with the regular expression
# The validity is set as True, and if the match fails (is None) it becomes False
def is_valid_ip(ip: str):
    valid_flag = True
    if type(ip) != str:
        valid_flag = False
    else:
        if re.match(valid_reg_ex, ip) is None:
            valid_flag = False
    return valid_flag


# Function that provides the class of the given address
# classless check is not implemented, assuming we are given only the ip address
# input is ip expected as string
def get_ip_class(ip):
    if type(ip) != str:
        print("Error: IP must be string")
        return

    # Ip validity check is performed
    if is_valid_ip(ip):

        # This is where classless check would be implemented
        classless = False
        if not classless:

            # since ip is valid, it is in the format of X1.X2.X3.X4 with acceptable values
            # we split in the dots and cast X1 into an integer in order to compare the value
            # and find the class
            first_byte = int(ip.split('.')[0])
            if first_byte < 128:
                class_type = 'A'
            elif first_byte < 192:
                class_type = 'B'
            elif first_byte < 224:
                class_type = 'C'
            elif first_byte < 240:
                class_type = 'D'
            else:
                class_type = 'E'

            # return text is formatted in each case
            # classful case
            return_text = "{X} is a class {Y} IP".format(X=ip, Y=class_type)
        else:
            # classless case
            return_text = "{X} is classless IP".format(X=ip)
    else:
        # invalid case
        return_text = "{X} is not a valid IP".format(X=ip)
    return return_text
