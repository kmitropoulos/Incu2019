import regex.remove_dup_words as rem

test_cases = {
    "this is is a this a simple is a this test case test case": "this is a simple test case",
    " ": "",
    """this can this this . be a a be can bigger test case 
    with case many with many more this can bigger be test
    with more many lines lines""": "this can be a bigger test case with many more lines"
}


def test_dup():
    for text in test_cases.keys():
        assert test_cases[text] == rem.remove_duplicates(text)
