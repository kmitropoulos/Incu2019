import regex.ip_validator as val

test_cases_valid = {
    "192.168.1.1": True,
    "255.255.255.255": True,
    "0.0.0.0": True,
    "124.124.02.2": False,
    "1-1-1-1": False,
    "4..8.1.2": False,
    "2 .2.1.4": False,
    "1.1.1.": False
}

test_cases_class = {
    "192.168.1.1": "192.168.1.1 is a class C IP",
    "255.255.255.255": "255.255.255.255 is a class E IP",
    "0.0.0.0": "0.0.0.0 is a class A IP",
    "239.23.1.1": "239.23.1.1 is a class D IP",
    "124.124.02.2": "124.124.02.2 is not a valid IP",
    "1-1-1-1": "1-1-1-1 is not a valid IP"
}


def test_valid():
    for ip in test_cases_valid.keys():
        assert val.is_valid_ip(ip) == test_cases_valid[ip]


def test_class():
    for ip in test_cases_class.keys():
        assert val.get_ip_class(ip) == test_cases_class[ip]