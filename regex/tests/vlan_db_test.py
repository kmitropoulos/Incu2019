import regex.vlan_db as vlan

test = """VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Po213, Eth1/1, Eth1/2, Eth1/3
                                                Eth1/4
10   Vlan10                           active    Po1, Po10, Po111, Po213, Eth1/2
                                                Eth1/5, Eth1/16, Eth1/17
                                                Eth1/18, Eth1/49, Eth1/50"""
ans = {'1': {'Name': 'default', 'Status': 'active', 'Ports': ['Po213', 'Eth1/1', 'Eth1/2', 'Eth1/3', 'Eth1/4']}, '10': {'Name': 'Vlan10', 'Status': 'active', 'Ports': ['Po1', 'Po10', 'Po111', 'Po213', 'Eth1/2', 'Eth1/5', 'Eth1/16', 'Eth1/17', 'Eth1/18', 'Eth1/49', 'Eth1/50']}}

assert vlan.get_vlan_db(test) == ans
