import regex.find_crc_types as fin
ans = {'Eth1/1': {'Align-Err': '0', 'FCS-Err': '0', 'Xmit-Err': '0', 'Rcv-Err': '0', 'UnderSize': '0', 'OutDiscards': '0', 'Single-Col': '0', 'Multi-Col': '0', 'Late-Col': '0', 'Exces-Col': '0', 'Carri-Sen': '0', 'Runts': '0', 'Giants': '0', 'SQETest-Err': '--', 'Deferred-Tx': '0', 'IntMacTx-Er': '0', 'IntMacRx-Er': '0', 'Symbol-Err': '0'}, 'Eth1/2': {'Align-Err': '0', 'FCS-Err': '0', 'Xmit-Err': '0', 'Rcv-Err': '0', 'UnderSize': '0', 'OutDiscards': '0', 'Single-Col': '0', 'Multi-Col': '0', 'Late-Col': '0', 'Exces-Col': '0', 'Carri-Sen': '0', 'Runts': '0', 'Giants': '0', 'SQETest-Err': '--', 'Deferred-Tx': '0', 'IntMacTx-Er': '0', 'IntMacRx-Er': '0', 'Symbol-Err': '0'}, 'Eth1/3': {'Align-Err': '0', 'FCS-Err': '0', 'Xmit-Err': '0', 'Rcv-Err': '0', 'UnderSize': '0', 'OutDiscards': '0', 'Single-Col': '0', 'Multi-Col': '0', 'Late-Col': '0', 'Exces-Col': '0', 'Carri-Sen': '0', 'Runts': '0', 'Giants': '0', 'SQETest-Err': '--', 'Deferred-Tx': '0', 'IntMacTx-Er': '0', 'IntMacRx-Er': '0', 'Symbol-Err': '0'}, 'Eth1/4': {'Align-Err': '0', 'FCS-Err': '0', 'Xmit-Err': '0', 'Rcv-Err': '0', 'UnderSize': '0', 'OutDiscards': '0', 'Single-Col': '0', 'Multi-Col': '0', 'Late-Col': '0', 'Exces-Col': '0', 'Carri-Sen': '0', 'Runts': '0', 'Giants': '0', 'SQETest-Err': '--', 'Deferred-Tx': '0', 'IntMacTx-Er': '0', 'IntMacRx-Er': '0', 'Symbol-Err': '0'}}
test = """--------------------------------------------------------------------------------
Port          Align-Err    FCS-Err   Xmit-Err    Rcv-Err  UnderSize OutDiscards
--------------------------------------------------------------------------------
Eth1/1                0          0          0          0          0           0
Eth1/2                0          0          0          0          0           0
Eth1/3                0          0          0          0          0           0
Eth1/4                0          0          0          0          0           0

--------------------------------------------------------------------------------
Port         Single-Col  Multi-Col   Late-Col  Exces-Col  Carri-Sen       Runts
--------------------------------------------------------------------------------
Eth1/1                0          0          0          0          0           0
Eth1/2                0          0          0          0          0           0
Eth1/3                0          0          0          0          0           0
Eth1/4                0          0          0          0          0           0

--------------------------------------------------------------------------------
Port          Giants SQETest-Err Deferred-Tx IntMacTx-Er IntMacRx-Er Symbol-Err
--------------------------------------------------------------------------------
Eth1/1             0          --           0           0           0          0
Eth1/2             0          --           0           0           0          0
Eth1/3             0          --           0           0           0          0
Eth1/4             0          --           0           0           0          0
"""

assert fin.get_error_counters(test) == ans
