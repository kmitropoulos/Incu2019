import re

###################################################
# Parse vlan status
# 
# Students will need to parse the output of a "show vlan" from a NXOS devices
# and return a dictionary with the parsed info
# 
# How the text input will look like:
# 
# 
# VLAN Name                             Status    Ports
# ---- -------------------------------- --------- -------------------------------
# 1    default                          active    Po213, Eth1/1, Eth1/2, Eth1/3
#                                                 Eth1/4
# 10   Vlan10                           active    Po1, Po10, Po111, Po213, Eth1/2
#                                                 Eth1/5, Eth1/16, Eth1/17
#                                                 Eth1/18, Eth1/49, Eth1/50
# How the output must look like:
# 
# vlan_db = {
# 	"1": {
# 		"Name": "default",
# 		"Status": "active",,
# 		"Ports": ["Po213", "Eth1/1"...]
# 	},
# 	"2":{},
# 	...
# }
#
###################################################


def get_vlan_db(text):
    vlan_db = {}  # the dictionary which will contain the needed data
    current_vlan = 0  # keep track of the current vlan
    port_list = []  # and the ports assigned to the current vlan

    for line in text.split('\n'):

        # if line is empty or starts with VLAN or --- skip
        if re.match(r"VLAN|--|^$", line) is not None:
            continue

        # if line starts with number it means that this is a new vlan
        elif re.match(r"^\d+", line) is not None:

            if current_vlan != 0:  # check for the first iteration
                # we must assign the ports to the previous vlan before we configure the next one
                vlan_db[str(current_vlan)]["Ports"] = port_list

            # get the data from the vlan line
            line_data = re.findall(r"\b\S+\b", line)
            current_vlan = line_data[0]  # assign current vlan
            # and initialize it in the dictionary with its name and status
            vlan_db[str(current_vlan)] = {"Name": line_data[1], "Status": line_data[2]}

            # keep the remaining ports in the port list
            port_list = line_data[3:]
        else:  # read the ports from the line and add them to the port list for the current vlan
            port_list += re.findall(r"\b\S+\b", line)

    # add the port list to the last vlan before we return the dictionary
    vlan_db[str(current_vlan)]["Ports"] = port_list

    return vlan_db

